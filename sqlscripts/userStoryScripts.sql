SELECT * FROM db_grad.deal;

SELECT instrument_name,deal_type, AVG(deal_amount) as AVGg FROM db_grad.deal
Group by instrument_name,deal_type;

-- profit of every dealer 
-- user stoy 3/4
-- does deal amount has to be multiplied with quantity?
Select 	a.counterparty_name Dealer,
		SUM(IF(deal_type ="S",a.AVGg,(-1)*a.AVGg)) Profit 
From (	Select counterparty_name,deal_type,
		SUM(deal_amount*deal_quantity) AVGg 
		From db_grad.deal
		Group by 	counterparty_name,
					deal_type
		) as a
Group by a.counterparty_name;


##  net trade amount quantity buy-sell
Select 	a.counterparty_name Dealer,
		SUM(IF(deal_type ="B",a.Quantity_sum,(-1)*a.Quantity_sum)) net_quantity
FROM(
Select counterparty_name,deal_type,
		SUM(deal_quantity) Quantity_sum
		From db_grad.deal
		Group by 	counterparty_name,
					deal_type
                    
	) as a
GROUP BY a.counterparty_name;

## number of trades done per trader

Select counterparty_name, deal_type, count(*) Number_trades FROM db_grad.deal
GROUP BY 1,2;

Select counterparty_name,deal_type,instrument_name, deal_amount, deal_quantity,
		(deal_amount*deal_quantity) deal_val 
		From db_grad.deal
		Group by 	counterparty_name,
					deal_type,
                    instrument_name;

Select 	a.counterparty_name Dealer,a.instrument_name,
		SUM(IF(deal_type ="S",a.AVGg,(-1)*a.AVGg)) Ending_Position 
From (	Select counterparty_name,deal_type,instrument_name,
		SUM(deal_amount*deal_quantity) AVGg 
		From db_grad.deal
		Group by 	counterparty_name,
					deal_type,
                    instrument_name
		) as a
Group by a.counterparty_name,
			a.instrument_name;
##User story 2


####Altered for the new schema provided by selvyn
#### Uses the view "dataView" to create the view

Select 	a.counterparty_name Dealer,a.instrument_name,
		SUM(IF(deal_type ="S",a.AVGg,(-1)*a.AVGg)) Ending_Position 
From (	Select counterparty_name,deal_type,instrument_name,
		SUM(deal_amount*deal_quantity) AVGg 
		From db_grad_cs_1917.dataView
		Group by 	counterparty_name,
					deal_type,
                    instrument_name
		) as a
Group by a.counterparty_name,
			a.instrument_name;
            