## Query for additional requirement 1

SELECT 
    instrument_name, 
    sum( if( deal_type = 'B', ROUND(AVGg,2), 0 ) ) AS Buy,  
    sum( if( deal_type = 'S', ROUND(AVGg,2), 0 ) ) AS Sell
FROM 
    (SELECT instrument_name,deal_type,
	AVG(deal_amount) as AVGg FROM db_grad_cs_1917.deal_view
	Group by instrument_name,deal_type
    ) as a
GROUP BY 
    instrument_name;
    
    
## Query for additional requirement 2 
## REPLACE counterparty name in Where clause with parameter!

Select 	a.counterparty_name Dealer,instrument_name,
		SUM(IF(deal_type ="B",a.Quantity_sum,(-1)*a.Quantity_sum)) net_quantity
FROM(
Select counterparty_name,deal_type,instrument_name,
		SUM(deal_quantity) Quantity_sum
		From db_grad_cs_1917.deal_view
        Where counterparty_name ="Estelle"
		Group by 	counterparty_name,instrument_name,
					deal_type
                    
	) as a
GROUP BY a.counterparty_name,instrument_name;    


## Query for additional requirement 3
select b.counterparty_name as Dealer, sum(b.profit) as Ending_Position from (
Select a.counterparty_name,a.instrument_name, MIN(q) * SUM(IF(deal_type ="S",a.avg_price,(-1)*a.avg_price)) as profit FROM
(
select counterparty_name, SUM(deal_quantity) as q, SUM(deal_quantity * deal_amount) / SUM(deal_quantity) as avg_price, instrument_name, deal_type
from a13_deal_view
group by counterparty_name, instrument_name, deal_type) a
group by instrument_name,counterparty_name) b
group by b.counterparty_name
Order by 2 DESC;

## Query for timeline STR_TO_DATE(deal_time,('%Y-%m-%dT%h:%i:%s'))

##SELECT deal_time as Time, deal_type,deal_amount FROM db_grad_cs_1917.deal_view;

SELECT 
    dealTime, 
     if( deal_type = 'B', ROUND(deal_amount,2), 0 )  AS Buy,  
    if( deal_type = 'S', ROUND(deal_amount,2), 0 )  AS Sell
FROM 
    (SELECT deal_time as dealTime,deal_type,
	deal_amount  FROM db_grad_cs_1917.deal_view
    Where instrument_name="Astronomica"
    ) as a
;



##Select (60/1) * HOUR(STR_TO_DATE(deal_time,('%Y-%m-%dT%h:%i:%s')) ) + FLOOR( MINUTE(STR_TO_DATE(deal_time,('%Y-%m-%dT%h:%i:%s')))/1) as myTime,deal_type, deal_amount 
##from deal_view
##Where instrument_name="Astronomica";


## Details for every counterparty. Quantity by instrument
SELECT 
    instrument_name, 
    sum( if( deal_type = 'B', ROUND(a.quantity,2), 0 ))  AS Buy,  
    sum( if( deal_type = 'S', ROUND(a.quantity,2), 0 ))  AS Sell
FROm( 
SELECT
instrument_name,
deal_type,
sum(deal_quantity) as quantity
FROM db_grad_cs_1917.deal_view
Where counterparty_name = "Estelle"
Group By 1,2) as a
Group by instrument_name;

## list of counterparties
Select distinct counterparty_name from db_grad_cs_1917.deal_view;

## list of instruments
Select distinct instrument_name from db_grad_cs_1917.deal_view;

## All deals
Select * from db_grad_cs_1917.deal_view