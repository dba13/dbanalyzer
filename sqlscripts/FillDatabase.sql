
## Auto increment  counterparty first

Insert Into counterparty (counterparty_name,counterparty_status,counterparty_date_registered)
Select  Distinct counterparty_name,counterparty_status,counterparty_date_registered from dealOLD;

## Auto incroment instrument first
Insert Into instrument (instrument_name)
Select distinct instrument_name from dealOLD;


Insert Into deal (deal_id,deal_time,deal_counterparty_id,deal_instrument_id,deal_type,deal_amount,deal_quantity)
Select deal_id,deal_time,counterparty_id,instrument_id,deal_type,deal_amount,deal_quantity
 From dealOLD dold Left JOIN instrument i on dold.instrument_name=i.instrument_name Left JOIN
		counterparty c on dold.counterparty_name=c.counterparty_name and dold.counterparty_status=c.counterparty_status 
        AND dold.counterparty_date_registered=c.counterparty_date_registered;
        
            
 CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`%` 
    SQL SECURITY DEFINER
VIEW `deal_view` AS
    SELECT 
        `d`.`deal_id` AS `deal_id`,
        `d`.`deal_time` AS `deal_time`,
        `d`.`deal_type` AS `deal_type`,
        `d`.`deal_amount` AS `deal_amount`,
        `d`.`deal_quantity` AS `deal_quantity`,
        `c`.`counterparty_name` AS `counterparty_name`,
        `c`.`counterparty_status` AS `counterparty_status`,
        `c`.`counterparty_date_registered` AS `counterparty_date_registered`,
        `i`.`instrument_name` AS `instrument_name`
    FROM
        ((`deal` `d`
        LEFT JOIN `counterparty` `c` ON ((`d`.`deal_counterparty_id` = `c`.`counterparty_id`)))
        LEFT JOIN `instrument` `i` ON ((`i`.`instrument_id` = `d`.`deal_instrument_id`)))           