DROP TABLE IF EXISTS dealMetadata;
DROP TABLE IF EXISTS counterParty;
DROP TABLE IF EXISTS instrument;
DROP TABLE IF EXISTS dealInfo;

CREATE TABLE counterParty (
    CP_ID INT(100) NOT NULL AUTO_INCREMENT,
    CP_Name VARCHAR(20),
    CP_Date DATETIME,
    CP_Status VARCHAR(5),
    PRIMARY KEY (CP_ID)
);
INSERT INTO counterParty SELECT DISTINCT NULL,counterparty_name, counterparty_date_registered, counterparty_status FROM deal;


CREATE TABLE instrument (
    Instrument_ID INT(100) NOT NULL AUTO_INCREMENT,
    Instrument_Name VARCHAR(35),
    PRIMARY KEY (Instrument_ID , Instrument_Name)
);
INSERT INTO instrument SELECT DISTINCT NULL, instrument_name FROM deal;

CREATE TABLE dealInfo (
    Deal_ID INT(11),
    Deal_Time VARCHAR(30),
    Deal_Type CHAR(1),
    Deal_Amount DECIMAL(12 , 2 ),
    Deal_Quantity INT(11),
    PRIMARY KEY (Deal_ID)
);
INSERT INTO dealInfo SELECT
	deal_id,
    deal_time,
    deal_type,
    deal_amount,
	deal_quantity
    FROM deal;
    
CREATE TABLE dealMetadata (
	Deal_ID INT(11),
    Instrument_ID INT (100),
    CP_ID INT (100),
    PRIMARY KEY(Deal_ID),
    FOREIGN KEY(Deal_ID) REFERENCES dealInfo(Deal_ID),
    FOREIGN KEY(Instrument_ID) REFERENCES instrument(Instrument_ID),
    FOREIGN KEY(CP_ID) REFERENCES counterParty(CP_ID)
);
INSERT INTO dealMetadata SELECT DISTINCT
	D.Deal_ID,
    I.Instrument_ID,
    C.CP_ID
    FROM dealInfo D, instrument I, counterParty C, deal D1
    WHERE D.Deal_ID = D1.deal_id AND I.Instrument_Name = D1.instrument_name AND C.CP_Name = D1.counterparty_name;