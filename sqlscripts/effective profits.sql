## replace  counterparty name in where clause in subselect
## and replace deal_type in where clause
Select Round(AVG(a.deal_amount),2),b.instrument_name from a13_deal_view a JOIN
(
Select  counterparty_name,instrument_name,deal_type, max(deal_time) as time from db_grad_cs_1917.a13_deal_view 
Where counterparty_name ="Estelle"
Group by counterparty_name,instrument_name,deal_type 
)  b ON a.instrument_name=b.instrument_name and a.deal_type=b.deal_type and b.time=a.deal_time and a.counterparty_name= b.counterparty_name
Where a.deal_type = "B"
group by instrument_name;