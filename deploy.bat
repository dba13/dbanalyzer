pushd %~dp0
call mvn package -fn -q
cd .\demowebapp\target
del C:\vm_share\tomcat-webapps\dbanalyzer-a1-3.war
xcopy dbanalyzer-a1-3.war C:\vm_share\tomcat-webapps /q /y
cd C:\vm_share\
docker stop t9s-dbda-91
docker stop t9s-dbda-92
docker rm t9s-dbda-91
docker rm t9s-dbda-92
rem docker run -p 8091:8080 -dit --name t9s-dbda-91 -v c:\vm_share\tomcat-webapps\dbanalyzer-a1-3.war:/usr/local/tomcat/webapps/dbanalyzer-a1-3.war tomcat9-server
rem docker run -p 8092:8080 -dit --name t9s-dbda-92 -v c:\vm_share\tomcat-webapps/dbanalyzer-a1-3.war:/usr/local/tomcat/webapps/dbanalyzer-a1-3.war tomcat9-server
docker run -p 8091:8080 -dit --name t9s-dbda-91 tomcat9-server
docker run -p 8092:8080 -dit --name t9s-dbda-92  tomcat9-server
timeout 5 /NOBREAK
cd C:\vm_share\tomcat-webapps
docker cp dbanalyzer-a1-3.war  t9s-dbda-91:/usr/local/tomcat/webapps
docker cp dbanalyzer-a1-3.war  t9s-dbda-92:/usr/local/tomcat/webapps
timeout 10 /NOBREAK
start chrome http:/localhost:8091/dbanalyzer-a1-3
pause