package com.db.demomidtier;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class DBConnectorTest {

    @Mock
    private Connection jdbcConnection;

    @Mock
    private ResultSet resultSet;

    @Mock
    private Statement statement;

    @Before
    public void setUp() throws Exception {
        resultSet = Mockito.mock(ResultSet.class);
        Mockito.when(resultSet.next()).thenReturn(true).thenReturn(false);
        Mockito.when(resultSet.getString(1)).thenReturn("table_r3").thenReturn("table_r1").thenReturn("table_r2");

        statement = Mockito.mock(Statement.class);
        Mockito.when(statement.executeQuery("SELECT name FROM tables")).thenReturn(resultSet);

        jdbcConnection = Mockito.mock(Connection.class);
        Mockito.when(jdbcConnection.createStatement()).thenReturn(statement);
        Mockito.when(jdbcConnection.isValid(1)).thenReturn(true);
    }

    @Test
    public void test_connect() {
        assertThat(DBConnector.getConnector().connect(jdbcConnection),is(true));
    }

    @Test
    public void test_isConnected() {
        DBConnector.getConnector().connect(jdbcConnection);
        assertThat(DBConnector.getConnector().isConnected(),is(true));
    }

    @Test
    public void test_isConnected_with_exception() throws SQLException {
        Mockito.reset(jdbcConnection);
        jdbcConnection = Mockito.mock(Connection.class);
        Mockito.when(jdbcConnection.createStatement()).thenReturn(statement);
        Mockito.when(jdbcConnection.isValid(1)).thenThrow(new SQLException());

        DBConnector.getConnector().connect(jdbcConnection);
        assertThat(DBConnector.getConnector().isConnected(),is(false));
    }
}