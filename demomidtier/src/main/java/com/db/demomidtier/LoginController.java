package com.db.demomidtier;

import org.json.JSONArray;

import javax.servlet.http.HttpServlet;
import java.sql.Connection;
import java.sql.SQLException;

public class LoginController extends HttpServlet {
    private Utils utils;

    public LoginController() {
        utils = new Utils();
    }

    public LoginController(Connection connection) {
        utils = new Utils(connection);
    }

    public boolean isConnected() {
        return utils.isConnected();
    }

    public String databaseAddress() {
        return utils.getDBAddress();
    }

    public String loginCheck(String user, String pass) {
        if (!isConnected()) {
            return null;
        }
        String queryString = "SELECT user_id, user_pwd FROM users WHERE user_id=? and user_pwd=?";
        try {
            JSONArray results = utils.executeSQL(queryString, user, pass);
            if (results == null || results.length() == 0) {
                return null;
            }
            String resultString = results.toString();
            int offset = 11;
            int start = resultString.indexOf("user_pwd") + offset;
            int end = resultString.indexOf("}") - 1;
            char[] resultStringArray = resultString.toCharArray();
            for (int i = start; i < end; i++) {
                resultStringArray[i] = '*';
            }
            return String.valueOf(resultStringArray);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

}
